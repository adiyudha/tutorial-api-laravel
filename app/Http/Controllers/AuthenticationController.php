<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthenticationController extends Controller
{
    public function login(Request $request)
    {
        // Data wajib input saat login
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        // Cek Apakah email ada
        $user = User::where('email', $request->email)->first();

        // Cek User tidak ada atau Password salah maka throw error
        if (! $user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        // Jika benar akan diberikan token
        return $user->createToken('user login')->plainTextToken;
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
    }

    public function me(Request $request)
    {
        // Retreive JSON - https://laravel.com/docs/9.x/responses#json-responses
        return response()->json(Auth::user());
    }
}
